package com.epam.latexml2openxml.console;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.w3c.dom.Document;

import com.epam.latexml2openxml.converters.presentation.PresentationConverter;
import com.epam.latexml2openxml.parser.IParser;
import com.epam.latexml2openxml.parser.Parser;
import com.epam.latexml2openxml.tokenizer.ITokenizer;
import com.epam.latexml2openxml.tokenizer.Tokenizer;
import com.epam.latexml2openxml.tokenizer.tokens.DocumentToken;
import com.epam.latexml2openxml.tokenizer.tokens.Token;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Console {
    
    /**
     * @param args
     * @throws IOException
     */
    @SuppressWarnings("static-access")
    public static void main(String[] args) throws IOException {
        Options options = new Options();
        
        Option helpOption = OptionBuilder.withDescription("Show this help")
                .withLongOpt("help").create("h");
        
        Option pptxOption = OptionBuilder.hasArg().withArgName("file")
                .withDescription("Creates OpenXML slideshow from input file")
                .withLongOpt("slideshow").create("s");
        
        Option pptxTemplateOption = OptionBuilder.hasArg().withArgName("file")
                .withDescription("POTX template")
                .withLongOpt("slideshow-template").create("st");
        
        options.addOption(helpOption);
        options.addOption(pptxOption);
        options.addOption(pptxTemplateOption);
        
        CommandLineParser parser = new GnuParser();
        
        try {
            CommandLine commandLine = parser.parse(options, args);
            
            if (commandLine.hasOption("h") || commandLine.getArgs().length != 1) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("latexml2openxml", options);
                return;
            }
            
            String[] files = commandLine.getArgs();
            if (files.length > 0) {
                File file = new File(files[0]);
                processFile(file, commandLine.hasOption("s"),
                        commandLine.getOptionValue("s"),
                        commandLine.hasOption("st"),
                        commandLine.getOptionValue("st"));
            }
            
        } catch (ParseException e) {
            System.err.println("Unable to parse command line");
            System.exit(1);
        }
        
    }
    
    private static void processFile(File file, boolean slideshowGenerate,
            String slideshowFilename, boolean slideshowTemplate,
            String slideshowTemplateFilename) throws IOException {
        IParser parser = new Parser();
        Document document = parser.Parse(file);
        
        ITokenizer tokenizer = new Tokenizer();
        Token token = tokenizer.Tokenize(document);
        if (token instanceof DocumentToken) {
            DocumentToken documentToken = (DocumentToken) token;
            if (slideshowGenerate) {
                FileOutputStream output = new FileOutputStream(
                        slideshowFilename);
                
                FileInputStream slideshowTemplateFile = slideshowTemplate ? new FileInputStream(
                        slideshowTemplateFilename) : null;
                
                PresentationConverter converter = new PresentationConverter();
                converter.convert(documentToken, output, slideshowTemplate,
                        slideshowTemplateFile);
            } else {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                System.out.println(gson.toJson(documentToken));
            }
        }
    }
    
}
