package com.epam.latexml2openxml.tokenizer;

import org.w3c.dom.Document;

import com.epam.latexml2openxml.tokenizer.tokens.DocumentToken;

public interface ITokenizer {
    public DocumentToken Tokenize(Document document);
}
