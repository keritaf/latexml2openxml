package com.epam.latexml2openxml.tokenizer;

import org.w3c.dom.Node;

import com.epam.latexml2openxml.tokenizer.tokens.BlockToken;
import com.epam.latexml2openxml.tokenizer.tokens.BreakToken;
import com.epam.latexml2openxml.tokenizer.tokens.ChapterToken;
import com.epam.latexml2openxml.tokenizer.tokens.ColumnToken;
import com.epam.latexml2openxml.tokenizer.tokens.CreatorToken;
import com.epam.latexml2openxml.tokenizer.tokens.DocumentToken;
import com.epam.latexml2openxml.tokenizer.tokens.EnumerateToken;
import com.epam.latexml2openxml.tokenizer.tokens.FrameTitleToken;
import com.epam.latexml2openxml.tokenizer.tokens.GraphicsToken;
import com.epam.latexml2openxml.tokenizer.tokens.ItemToken;
import com.epam.latexml2openxml.tokenizer.tokens.ItemizeToken;
import com.epam.latexml2openxml.tokenizer.tokens.LiteralToken;
import com.epam.latexml2openxml.tokenizer.tokens.ModeToken;
import com.epam.latexml2openxml.tokenizer.tokens.OnlyToken;
import com.epam.latexml2openxml.tokenizer.tokens.ParagraphToken;
import com.epam.latexml2openxml.tokenizer.tokens.ParaToken;
import com.epam.latexml2openxml.tokenizer.tokens.PauseToken;
import com.epam.latexml2openxml.tokenizer.tokens.PersonNameToken;
import com.epam.latexml2openxml.tokenizer.tokens.SectionToken;
import com.epam.latexml2openxml.tokenizer.tokens.SlideToken;
import com.epam.latexml2openxml.tokenizer.tokens.SubSectionToken;
import com.epam.latexml2openxml.tokenizer.tokens.TOCToken;
import com.epam.latexml2openxml.tokenizer.tokens.TagToken;
import com.epam.latexml2openxml.tokenizer.tokens.TextToken;
import com.epam.latexml2openxml.tokenizer.tokens.TitlePageToken;
import com.epam.latexml2openxml.tokenizer.tokens.TitleToken;
import com.epam.latexml2openxml.tokenizer.tokens.Token;
import com.epam.latexml2openxml.tokenizer.tokens.UnknownToken;
import com.epam.latexml2openxml.tokenizer.tokens.VerbatimToken;

public class NodeTokenizer {
    public static Token Tokenize(Node node) {
        if (node == null) {
            return null;
        }
        Token token = null;
        if (node.getNodeType() == Node.TEXT_NODE) {
            token = LiteralToken.Tokenize(node);
        }
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            token = TokenizeElementNode(node);
        }
        return token;
    }
    
    private static Token TokenizeElementNode(Node node) {
        if (node == null || node.getNodeType() != Node.ELEMENT_NODE) {
            return null;
        }
        
        switch (node.getNodeName()) {
            case DocumentToken.NodeName:
                return DocumentToken.Tokenize(node);
            case TitleToken.NodeName:
                return TitleToken.Tokenize(node);
            case TagToken.NodeName:
                return TagToken.Tokenize(node);
            case SectionToken.NodeName:
                return SectionToken.Tokenize(node);
            case SubSectionToken.NodeName:
                return SubSectionToken.Tokenize(node);
            case ChapterToken.NodeName:
                return ChapterToken.Tokenize(node);
            case SlideToken.NodeName:
                return SlideToken.Tokenize(node);
            case FrameTitleToken.NodeName:
                return FrameTitleToken.Tokenize(node);
            case ItemizeToken.NodeName:
                return ItemizeToken.Tokenize(node);
            case EnumerateToken.NodeName:
                return EnumerateToken.Tokenize(node);
            case ItemToken.NodeName:
                return ItemToken.Tokenize(node);
            case ModeToken.NodeName:
                return ModeToken.Tokenize(node);
            case OnlyToken.NodeName:
                return OnlyToken.Tokenize(node);
            case ParaToken.NodeName:
                return ParaToken.Tokenize(node);
            case ParagraphToken.NodeName:
                return ParagraphToken.Tokenize(node);
            case TextToken.NodeName:
                return TextToken.Tokenize(node);
            case CreatorToken.NodeName:
                return CreatorToken.Tokenize(node);
            case PersonNameToken.NodeName:
                return PersonNameToken.Tokenize(node);
            case TitlePageToken.NodeName:
                return TitlePageToken.Tokenize(node);
            case TOCToken.NodeName:
                return TOCToken.Tokenize(node);
            case BlockToken.NodeName:
                return BlockToken.Tokenize(node);
            case PauseToken.NodeName:
                return PauseToken.Tokenize(node);
            case ColumnToken.NodeName:
                return ColumnToken.Tokenize(node);
            case BreakToken.NodeName:
                return BreakToken.Tokenize(node);
            case GraphicsToken.NodeName:
                return GraphicsToken.Tokenize(node);
            case VerbatimToken.NodeName:
                return VerbatimToken.Tokenize(node);
            default:
                return UnknownToken.Tokenize(node);
        }
    }
    
}
