package com.epam.latexml2openxml.tokenizer.tokens;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.epam.latexml2openxml.tokenizer.NodeListTokenizer;

public abstract class Token {
    public static <T extends Token> T Tokenize(Node node, Class<T> tokenClass,
            String nodeName) {
        if (node == null) {
            return null;
        }
        if (node.getNodeType() != Node.ELEMENT_NODE) {
            return null;
        }
        if (node.getNodeName() != nodeName) {
            return null;
        }
        
        T token;
        try {
            token = tokenClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
        token.TokenizeChildNodes(node);
        token.TokenizeAttributes(node);
        
        return token;
    }
    
    public String                    Name;
    
    public List<Token>               ChildTokens = new ArrayList<Token>();
    
    public Hashtable<String, String> Attributes  = new Hashtable<String, String>();
    
    public void TokenizeAttributes(Node node) {
        Attributes.clear();
        if (node == null || node.getNodeType() != Node.ELEMENT_NODE) {
            return;
        }
        if (node.hasAttributes()) {
            NamedNodeMap attributes = node.getAttributes();
            
            for (int i = 0; i < attributes.getLength(); i++) {
                Node attribute = attributes.item(i);
                Attributes.put(attribute.getNodeName(),
                        attribute.getNodeValue());
            }
        }
    }
    
    public void TokenizeChildNodes(Node node) {
        ChildTokens.clear();
        if (node == null) {
            return;
        }
        if (node.hasChildNodes()) {
            ChildTokens = NodeListTokenizer.Tokenize(node.getChildNodes());
        }
    }
    
}
