package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class BreakToken extends Token {
    public final static String NodeName = "break";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, BreakToken.class, BreakToken.NodeName);
    }
    
    public BreakToken() {
        Name = NodeName;
    };
    
}
