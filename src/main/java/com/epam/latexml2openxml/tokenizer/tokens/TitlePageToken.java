package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class TitlePageToken extends Token {
    public static final String NodeName = "titlepage";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, TitlePageToken.class,
                TitlePageToken.NodeName);
    };
    
    public TitlePageToken() {
        Name = NodeName;
    }
}
