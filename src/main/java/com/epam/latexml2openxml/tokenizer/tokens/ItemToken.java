package com.epam.latexml2openxml.tokenizer.tokens;

import java.util.Iterator;

import org.w3c.dom.Node;

public class ItemToken extends Token {
    public final static String NodeName = "item";
    
    public static Token Tokenize(Node node) {
        ItemToken token = Token.Tokenize(node, ItemToken.class,
                ItemToken.NodeName);
        
        if (token != null && token.ChildTokens != null) {
            for (Iterator<Token> iterator = token.ChildTokens.iterator(); iterator
                    .hasNext();) {
                Token childToken = iterator.next();
                
                if (childToken instanceof TagToken) {
                    token.Tag = (TagToken) childToken;
                    iterator.remove();
                }
            }
        }
        return token;
    }
    
    public TagToken Tag;
    
    public ItemToken() {
        Name = NodeName;
    }
}
