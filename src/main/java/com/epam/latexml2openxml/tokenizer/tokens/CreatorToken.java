package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class CreatorToken extends Token {
    public final static String NodeName = "creator";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, CreatorToken.class, CreatorToken.NodeName);
    }
    
    public CreatorToken() {
        Name = NodeName;
    };
}
