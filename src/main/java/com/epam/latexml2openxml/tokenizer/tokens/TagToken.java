package com.epam.latexml2openxml.tokenizer.tokens;

import java.util.Iterator;

import org.w3c.dom.Node;

public class TagToken extends Token {
    public static final String NodeName = "tag";
    
    public static Token Tokenize(Node node) {
        TagToken token = Token
                .Tokenize(node, TagToken.class, TagToken.NodeName);
        
        if (token != null && token.ChildTokens != null) {
            for (Iterator<Token> iterator = token.ChildTokens.iterator(); iterator
                    .hasNext();) {
                Token childToken = iterator.next();
                
                if (childToken instanceof LiteralToken) {
                    token.Text = ((LiteralToken) childToken).Text;
                    iterator.remove();
                } else {
                    iterator.remove();
                }
            }
        }
        return token;
    }
    
    public String Text;
    
    public TagToken() {
        Name = NodeName;
    }
}
