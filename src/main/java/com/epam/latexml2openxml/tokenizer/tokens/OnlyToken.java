package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class OnlyToken extends Token {
    public static final String NodeName = "only";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, OnlyToken.class, OnlyToken.NodeName);
    }
    
    public OnlyToken() {
        Name = NodeName;
    };
}
