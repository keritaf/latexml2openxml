package com.epam.latexml2openxml.tokenizer.tokens;

import java.util.Iterator;

import org.w3c.dom.Node;

public class SectionToken extends Token {
    public final static String NodeName = "section";
    
    public static Token Tokenize(Node node) {
        SectionToken token = Tokenize(node, SectionToken.class,
                SectionToken.NodeName);
        if (token != null && token.ChildTokens != null) {
            for (Iterator<Token> iterator = token.ChildTokens.iterator(); iterator
                    .hasNext();) {
                Token childToken = iterator.next();
                
                if (childToken instanceof TitleToken) {
                    token.Title = (TitleToken) childToken;
                    iterator.remove();
                }
            }
        }
        
        return token;
    }
    
    public TitleToken Title;
    
    public SectionToken() {
        Name = NodeName;
    };
}
