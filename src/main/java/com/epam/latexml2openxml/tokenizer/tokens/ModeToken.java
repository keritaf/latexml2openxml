package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class ModeToken extends Token {
    public final static String NodeName = "mode";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, ModeToken.class, ModeToken.NodeName);
    }
    
    public ModeToken() {
        Name = NodeName;
    };
}
