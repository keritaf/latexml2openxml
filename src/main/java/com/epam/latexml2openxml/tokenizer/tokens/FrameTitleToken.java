package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class FrameTitleToken extends TitleToken {
    public static final String NodeName = "frametitle";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, FrameTitleToken.class,
                FrameTitleToken.NodeName);
    };
    
    public FrameTitleToken() {
        Name = NodeName;
    }
}
