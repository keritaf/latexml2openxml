package com.epam.latexml2openxml.parser;

import java.io.File;

import org.w3c.dom.Document;

public interface IParser {
    public Document Parse(File file);
}
