package com.epam.latexml2openxml.converters.presentation;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xslf.usermodel.*;

import com.epam.latexml2openxml.converters.image.ImageConverter;
import com.epam.latexml2openxml.converters.image.ImageFormat;
import com.epam.latexml2openxml.tokenizer.tokens.*;
import com.google.common.base.Strings;

public class PresentationConverter {
    public void convert(DocumentToken document, OutputStream outputStream,
            boolean hasTemplate, InputStream templateStream) throws IOException {
        if (document == null || outputStream == null) {
            return;
        }
        
        if (hasTemplate) {
            presentation = new XMLSlideShow(templateStream);
            presentation
                    .getPackage()
                    .replaceContentType(
                            "application/vnd.openxmlformats-officedocument.presentationml.template.main+xml",
                            "application/vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml");
        } else
            presentation = new XMLSlideShow();
        
        System.out.println("Available slide layouts:");
        for (XSLFSlideMaster master : presentation.getSlideMasters()) {
            for (XSLFSlideLayout layout : master.getSlideLayouts()) {
                System.out.println(layout.getType());
            }
        }
        
        master = presentation.getSlideMasters()[0];
        
        if (document.Title != null)
            createPresentationTitleSlide(document);
        createPresentationSlides(document);
        
        presentation.write(outputStream);
    }
    
    private void createPresentationSlides(DocumentToken document) {
        createPresentationSlidesInternal(document);
    }
    
    private void createPresentationSlidesInternal(Token token) {
        if (token == null || presentation == null || master == null)
            return;
        if (token instanceof LiteralToken)
            return;
        else if (token instanceof SlideToken)
            createPresentationSlide((SlideToken) token);
        else {
            for (Token childToken : token.ChildTokens) {
                createPresentationSlidesInternal(childToken);
            }
        }
        
    }
    
    private void createPresentationSlide(SlideToken slideToken) {
        if (slideToken == null || presentation == null || master == null)
            return;
        XSLFSlideLayout layout = null;
        if (slideToken.IsTitle) {
            layout = master.getLayout(SlideLayout.SECTION_HEADER);
        } else if (slideToken.ChildTokens.size() == 0) {
            layout = master.getLayout(SlideLayout.TITLE_ONLY);
        } else
            layout = master.getLayout(SlideLayout.TITLE_AND_CONTENT);
        
        createSlide(layout);
        
        XSLFTextShape titlePlaceholder = slide.getPlaceholder(0);
        if (slideToken.Title != null) {
            convertTokenToTextShape(slideToken.Title, titlePlaceholder);
        } else {
            titlePlaceholder.clearText();
        }
        
        if (slideToken.ChildTokens.size() != 0) {
            XSLFTextShape contentPlaceholder = slide.getPlaceholder(1);
            for (Token childToken : slideToken.ChildTokens) {
                convertTokenToTextShape(childToken, contentPlaceholder);
            }
        }
    }
    
    private void createSlide(XSLFSlideLayout layout) {
        slide = presentation.createSlide(layout);
        logSlideCreated();
    }
    
    private void createPresentationTitleSlide(DocumentToken document) {
        
        if (document == null)
            return;
        
        boolean hasSubtitle = document.Creator != null;
        XSLFSlideLayout titleLayout = master
                .getLayout(hasSubtitle ? SlideLayout.TITLE
                        : SlideLayout.TITLE_ONLY);
        
        createSlide(titleLayout);
        
        currentTextShape = slide.getPlaceholder(0);
        if (document.Title != null) {
            convertTokenToTextShape(document.Title, currentTextShape);
        } else {
            currentTextShape.clearText();
        }
        
        if (hasSubtitle) {
            currentTextShape = slide.getPlaceholder(1);
            convertTokenToTextShape(document.Creator, currentTextShape);
        }
        
    }
    
    private void logSlideCreated() {
        slideCounter++;
        System.out.print(String.format("[%d]", slideCounter));
    }
    
    private boolean convertTokenToTextShape(Token token, XSLFTextShape textShape) {
        if (token == null || textShape == null)
            return false;
        currentTextShape = textShape;
        currentIndentationLevel = -1;
        textShape.clearText();
        currentParagraph = null;
        boolean result = convertToken(token);
        textShape.setTextAutofit(TextAutofit.SHAPE);
        textShapeResizeToFitText(textShape);
        return result;
    }
    
    private boolean convertToken(Token token) {
        
        if (token == null || currentTextShape == null)
            return false;
        if (token instanceof SlideToken)
            createPresentationSlide((SlideToken) token);
        
        createParagraphIfNull();
        
        if (token instanceof LiteralToken) {
            createParagraphIfNull();
            XSLFTextRun currentTextRun = currentParagraph.addNewTextRun();
            currentTextRun.setText(((LiteralToken) token).Text);
        } else if (token instanceof TextToken) {
            createParagraphIfNull();
            TextToken textToken = (TextToken) token;
            processTextToken(textToken);
        } else if (token instanceof BreakToken) {
            //createParagraphIfNull();
            currentParagraph = createParagraph();
            //currentParagraph.addLineBreak();
        } else if (token instanceof ParaToken) {
            createParagraphIfNull();
            convertChildTokens(token);
        } else if (token instanceof ParagraphToken) {
            processParagraphToken(token);
        } else if (token instanceof ItemizeToken
                || token instanceof EnumerateToken) {
            createParagraphIfNull();
            currentIndentationLevel++;
            convertChildTokens(token);
            currentIndentationLevel--;
        } else if (token instanceof BlockToken) {
            createContinuationTextShape();
            convertChildTokens(token);
        } else if (token instanceof ColumnToken) {
            createContinuationTextShape();
        } else if (token instanceof GraphicsToken) {
            GraphicsToken graphicsToken = (GraphicsToken) token;
            if (!Strings.isNullOrEmpty(graphicsToken.Source)) {
                createImage(graphicsToken.Source);
            }
        } else if (token instanceof SectionToken) {
            createSectionSlide((SectionToken) token);
        } else if (token instanceof SubSectionToken) {
            createSubSectionSlide((SectionToken) token);
        } else if (token instanceof VerbatimToken) {
            processVerbatimToken((VerbatimToken) token);
        } else {
            createParagraphIfNull();
            convertChildTokens(token);
        }
        return true;
    }
    
    private void createParagraphIfNull() {
        if (currentParagraph == null) {
            currentParagraph = createParagraph();
        }
    }
    
    private void processVerbatimToken(VerbatimToken token) {
        createParagraph();
        String[] lines = token.Text.replaceAll("\n$","").split("\n");
        
        boolean hasFontAttribute = false;
        String fontFamily = "";
        if (token.Attributes.containsKey("font"))
        {
            String fontAttribute = token.Attributes.get("font");
            switch (fontAttribute) {
                case "typewriter":
                    fontFamily = "Courier New";
                    break;
                default:
                    hasFontAttribute = false;
            }
            
        }
        
        
        for (String line : lines) {
            if (!line.isEmpty())
            {
                XSLFTextRun run = currentParagraph.addNewTextRun();
                run.setText(line);
                if (hasFontAttribute)
                    run.setFontFamily(fontFamily);
            }
            currentParagraph.addLineBreak();
        }
    }
    
    private void processParagraphToken(Token token) {
        currentParagraph = createParagraph();
        String alignAttribute = token.Attributes.get("align");
        if (!Strings.isNullOrEmpty(alignAttribute)) {
            switch (alignAttribute) {
                case "center":
                    currentParagraph.setTextAlign(TextAlign.CENTER);
                    break;
                
                default:
                    break;
            }
        }
        convertChildTokens(token);
    }
    
    private void processTextToken(TextToken textToken) {
        if (!Strings.isNullOrEmpty(textToken.Text)) {
            XSLFTextRun currentTextRun = currentParagraph.addNewTextRun();
            currentTextRun.setText(textToken.Text);
            String fontAttribute = textToken.Attributes.get("font");
            if (!Strings.isNullOrEmpty(fontAttribute)) {
                switch (fontAttribute) {
                    case "bold":
                        currentTextRun.setBold(true);
                        break;
                    case "typewriter":
                        currentTextRun.setFontFamily("Courier New");
                        break;
                    case "italic":
                        currentTextRun.setItalic(true);
                        break;
                    default:
                        break;
                }
            }
            String colorAttribute = textToken.Attributes.get("color");
            if (!Strings.isNullOrEmpty(colorAttribute)) {
                currentTextRun.setFontColor(PresentationConverterUtils
                        .parseColor(colorAttribute));
            }
        }
        convertChildTokens(textToken);
    }
    
    private void createSubSectionSlide(SectionToken token) {
        createSlide(master.getLayout(SlideLayout.SECTION_HEADER));
        convertTokenToTextShape(token.Title, slide.getPlaceholder(0));
        convertChildTokens(token);
    }
    
    private void createSectionSlide(SectionToken token) {
        createSlide(master.getLayout(SlideLayout.SECTION_HEADER));
        convertTokenToTextShape(token.Title, slide.getPlaceholder(0));
        convertChildTokens(token);
    }
    
    private void createImage(String imageFilename) {
        File graphicsSourceFile = new File(imageFilename);
        if (graphicsSourceFile.isFile()) {
            byte[] pictureData = null;
            try {
                String mimeType = Files.probeContentType(graphicsSourceFile
                        .toPath());
                if (mimeType.compareToIgnoreCase("image/png") == 0) {
                    pictureData = IOUtils.toByteArray(new FileInputStream(
                            imageFilename));
                } else {
                    String convertedImageFilename = imageFilename + ".png";
                    ImageConverter.ConvertImageFile(imageFilename,
                            convertedImageFilename, ImageFormat.PNG);
                    pictureData = IOUtils.toByteArray(new FileInputStream(
                            convertedImageFilename));
                }
            } catch (IOException e) {
                // e.printStackTrace();
            }
            if (pictureData != null) {
                int idx = presentation.addPicture(pictureData,
                        XSLFPictureData.PICTURE_TYPE_PNG);
                XSLFPictureShape picture = slide.createPicture(idx);
                // Rectangle2D currentAnchor = currentTextShape.getAnchor();
                // picture.setAnchor(new
                // Rectangle2D.Double(currentAnchor.getX(),
                // currentAnchor.getY(), 200, 200));
                // picture.resize();
            }
        }
    }
    
    private XSLFTextParagraph createParagraph() {
        XSLFTextParagraph paragraph = currentTextShape.addNewTextParagraph();
        paragraph
                .setLevel(currentIndentationLevel >= 0 ? currentIndentationLevel
                        : 0);
        paragraph.setBullet(currentIndentationLevel >= 0);
        return paragraph;
    }
    
    private void createContinuationTextShape() {
        if (currentParagraph != null) {
            textShapeResizeToFitText(currentTextShape);
            Rectangle2D oldAnchor = currentTextShape.getAnchor();
            Rectangle2D newAnchor = new Rectangle2D.Double(oldAnchor.getX(),
                    oldAnchor.getY() + oldAnchor.getHeight(),
                    oldAnchor.getWidth(), oldAnchor.getHeight());
            
            currentTextShape = slide.createTextBox();
            currentTextShape.setAnchor(newAnchor);
            currentTextShape.setPlaceholder(Placeholder.CONTENT);
            currentTextShape.clearText();
            currentParagraph = null;
        }
    }
    
    private void textShapeResizeToFitText(XSLFTextShape shape) {
        try {
            shape.resizeToFitText();
        } catch (Exception e) {
            // System.err.println("Error resizing text.");
            // e.printStackTrace(System.err);
        }
    }
    
    private void convertChildTokens(Token token) {
        for (Token childToken : token.ChildTokens) {
            convertToken(childToken);
        }
    }
    
    private XSLFSlideMaster   master;
    private XMLSlideShow      presentation;
    private XSLFSlide         slide;
    private XSLFTextShape     currentTextShape;
    private XSLFTextParagraph currentParagraph;
    
    private int               slideCounter = 0;
    private int               currentIndentationLevel;
    
}
